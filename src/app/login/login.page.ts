import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username: string;
  password: string;
  constructor(
    public alertController: AlertController,
    public router: Router,
    private nav: NavController
  ) { }

  ngOnInit() {
  }

  async presentAlertFail() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Invalid Credentials',
      message: 'Please try again!',
      buttons: ['OK']
    });

    await alert.present();
  }

  //User login function with hardcoded values
  login() {
    if(this.username == "user" && this.password == "user") {
      this.nav.navigateRoot('/employees');
    } else {
      this.presentAlertFail();
    }
  }
}
