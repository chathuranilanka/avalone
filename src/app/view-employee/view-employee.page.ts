import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { Location } from "@angular/common";

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.page.html',
  styleUrls: ['./view-employee.page.scss'],
})
export class ViewEmployeePage implements OnInit {

  id:any = 0;
  employees: any = [];
  employee: any = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    public alertController: AlertController,
    private storage: Storage,
    public router: Router,
    private location: Location
  ) {
    this.id = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
   }
  

  ngOnInit() {
    //Get the employees data from storage
    this.storage.get('employees').then((val) => {
      this.employees = val;
      this.employees.forEach(element => {
        if(element.id == this.id){
          this.employee = element;
        }
      });
    });
  }

  ionViewWillEnter() {
    this.ngOnInit();
  }

  //Alert controller for delete employee confirmation and delete employee
  async presentAlertDelete() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Warning!',
      message: 'Do you want to delete this employee?',
      buttons: [
       {
          text: 'Yes',
          handler: () => {
            //Delete the employee and save in the storage
            //Filter the employee using filter function
            this.employees = this.employees.filter(item=>item.id != this.id );
            this.storage.set('employees', this.employees);
            this.location.back();
          }
        },
        {
          text: 'No',
          handler: () => {}
        }
      ]
    });

    await alert.present();
  }

  //Update function for navigate to edit employee page
  update() {
    this.router.navigate(['/', 'edit-employee', this.id ]);
  }

  //Delete function for delete the employee
  delete() {
    this.presentAlertDelete();
  }
}
