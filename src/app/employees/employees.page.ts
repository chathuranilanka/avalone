import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.page.html',
  styleUrls: ['./employees.page.scss'],
})
export class EmployeesPage implements OnInit {

  employees: any = [];

  constructor(
    public toastController: ToastController,
    private storage: Storage,
    public router: Router,
  ) { }

  ngOnInit() {
    //Get the employees data from storage
    this.storage.get('employees').then((val) => {
      this.employees = val;
      if(this.employees == null) {
        this.presentToast();
      }
    });
  } 

  ionViewWillEnter() {
    this.ngOnInit();
  }

  //Toast message for error when there is no any employees on storage
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'There is no any employees..Please add new employee!',
      duration: 3000
    });
    toast.present();
  }

  //Add function for navigate to employee create page
  add(){
    this.router.navigate(['/', 'add-employee']);
  }

  //viewEmployee function for navigate to view employee page
  viewEmployee(id) {
    this.router.navigate(['/', 'view-employee', id ]);
  }
}
