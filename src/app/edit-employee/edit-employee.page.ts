import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Location } from "@angular/common";

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.page.html',
  styleUrls: ['./edit-employee.page.scss'],
})
export class EditEmployeePage implements OnInit {

  id:any = 0;
  employees: any = [];
  employee: any = {};

  constructor(
    public alertController: AlertController,
    private activatedRoute: ActivatedRoute,
    private storage: Storage,
    public router: Router,
    private location: Location
  ) {
    this.id = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
   }
  

  ngOnInit() {
    //Get the employees data from storage
    this.storage.get('employees').then((val) => {
      this.employees = val;
      this.employees.forEach(element => {
        if(element.id == this.id){
          this.employee = element;
        }
      });
    });
  }

  //Alert controller for display success message when employee updated
  async presentAlertSuccess() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Success!',
      message: 'Employee updated successfully!',
      buttons: [
       {
          text: 'Ok',
          handler: () => {
            this.location.back();
          }
        }
      ]
    });

    await alert.present();
  }

  //Alert controller for display error message when employee update
  async presentAlertFail() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Missing Fields',
      message: 'Please check again!',
      buttons: ['OK']
    });

    await alert.present();
  }

  update(){
    //Check the input fields null or not
    if(this.employee.fName && this.employee.lName && this.employee.gender && this.employee.birthday && this.employee.email && this.employee.number && this.employee.address && this.employee.status) {
      //Filter the specific employee using filter method
      this.employees = this.employees.filter(item=>item.id != this.id );
      this.employees.push(this.employee);
      this.storage.set('employees', this.employees);
      this.presentAlertSuccess();
    } else {
      this.presentAlertFail()
    }
    
  }
}
