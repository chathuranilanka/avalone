import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Location } from "@angular/common";

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.page.html',
  styleUrls: ['./add-employee.page.scss'],
})
export class AddEmployeePage implements OnInit {

  employees: any = [];
  employee: any = {};

  id: number = 0;
  fName: string = null;
  lName: string = null;
  gender: any = null;
  birthday: any = null;
  email: string = null;
  number: string = null;
  address: string = null;
  status: any = null;


  constructor(
    public alertController: AlertController,
    private storage: Storage,
    public router: Router,
    private location: Location
  ) { }

  ngOnInit() {
    //Get the employees data from storage
    this.storage.get('employees').then((val) => {
      this.employees = val;
    });
    //Get the employees id data from storage
    this.storage.get('id').then((val) => {
      this.id = val;
    });
  }
  //Alert controller for display success message when new employee created
  async presentAlertSuccess() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Success!',
      message: 'New employee added successfully!',
      buttons: [
       {
          text: 'Ok',
          handler: () => {
            this.location.back();
          }
        }
      ]
    });

    await alert.present();
  }

  //Alert controller for display error message when new employee create
  async presentAlertFail() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Missing Fields',
      message: 'Please check again!',
      buttons: ['OK']
    });

    await alert.present();
  }

  create(){
    //Check the input fields null or not
    if(this.fName && this.lName && this.gender && this.birthday && this.email && this.number && this.address && this.status) {
      this.id++;
      //Create the employee object
      this.employee = {
        id: this.id,
        fName: this.fName,
        lName: this.lName,
        gender: this.gender,
        birthday: this.birthday,
        email: this.email,
        number: this.number,
        address: this.address,
        status: this.status
      }
      //Create and save the new employee in the storage 
      if(this.employees == null) {
        this.employees = [
          this.employee
        ];
        this.storage.set('id', 1);
      } else {
        this.storage.set('id', this.id);
        this.employees.push(this.employee);
      }
      this.storage.set('employees', this.employees);
      this.presentAlertSuccess();
      //Assign null values for input fields
      this.fName = this.lName = this.gender = this.birthday = this.email = this.number = this.address = this.status = null
    
    } else {
      this.presentAlertFail()
    }
  }
}
